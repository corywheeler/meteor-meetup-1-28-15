# Meteor Meetup 1/28/15 #



### This is the code that Sean produced while presenting at the meetup. ###

### How do I get set up? ###

* First install Meteor by following the instructions found [ here ](https://www.meteor.com/install)
* You can clone this repo down by typing on your command line: git clone git@bitbucket.org:corywheeler/meteor-meetup-1-28-15.git
* From the command line, change your directory into the folder that was created and type: meteor
* You should see this message once the app has spun up: App running at: http://localhost:3000/
* Open a web browser and put the url http://localhost:3000/ in your browser and you should see in the upper left hand corner the button that you can click to add a new timer into the DOM.

### Now experiment with Meteor, and enjoy! ###