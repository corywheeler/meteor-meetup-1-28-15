Dependency = function() {
   // this = {}
   var that = this;
   
   this.List = [];

   this.changed = function() {
      // invalidate functions...
      that.List.forEach(function(computation){
         computation.invalidate();
      });
      that.List = [];
   };

   this.depend = function(computation) {
      console.log("Adding dep");
      that.List.push(computation);
   }

   this.hasDependents = function() {
      return that.List.length > 0;
   }

   return this;
}


var Computation = function(fn) {
   this.invalidate = function() {
      console.log("At invalidate");
      this.onInvalidate();
   };

   this.onInvalidate = function() {
      console.log("At onInvalidate");
      fn();
      Computation(fn);
   };

   return this;
}

var dep = new Dependency();

var comp = new Computation( function() {
   console.log("Changed");
   dep.depend(this);
});

dep.depend(comp);
console.log("Hello");
dep.changed();

setTimeout(function(){ dep.changed();}, 10000);


