Template.main.created = function() {
   var that = this;
   this.result = new ReactiveVar(0);

   this.interval = Meteor.setInterval(function(){
      that.result.set(that.result.get()+1);
   },1000);
}

Template.main.destroyed = function() {
   Meteor.clearInterval(this.interval);
}

Template.main.helpers({
   'result': function() {
      return Template.instance().result.get();
      //return result.get()'result');
   },
})

